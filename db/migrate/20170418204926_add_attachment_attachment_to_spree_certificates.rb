class AddAttachmentAttachmentToSpreeCertificates < ActiveRecord::Migration
  def self.up
    change_table :spree_certificates do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :spree_certificates, :attachment
  end
end
