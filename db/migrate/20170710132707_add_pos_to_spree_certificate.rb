class AddPosToSpreeCertificate < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_certificates, :position, :integer
  end
end
