class AddUserToSpreeCertificate < ActiveRecord::Migration[5.0]
  def change
    add_reference :spree_certificates, :user, index: true, foreign_key: { to_table: :spree_users }
  end
end
