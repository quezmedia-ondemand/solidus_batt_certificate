class AddOwnerToSpreeCertificate < ActiveRecord::Migration[5.0]
  def change
    add_reference :spree_certificates, :owner, polymorphic: true
  end
end
