class AddDefPosToSpreeCertificate < ActiveRecord::Migration[5.0]
  def change
    change_column :spree_certificates, :position, :integer, default: 1
  end
end
