class CreateSpreeCertificates < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_certificates do |t|
      t.string :name

      t.timestamps
    end
  end
end
