class AddAvToSpreeCertificate < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_certificates, :available, :boolean
  end
end
