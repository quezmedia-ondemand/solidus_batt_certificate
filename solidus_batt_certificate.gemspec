# encoding: UTF-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'solidus_batt_certificate/version'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'solidus_batt_certificate'
  s.version     = '1.0'
  s.summary     = 'Basic Product Certificate File Upload'
  s.description = 'It allows the admin to upload a certification file for products'
  s.license     = 'BSD-3-Clause'
  s.required_ruby_version = ">= 2.1"

  s.author    = 'Hugo Hernani'
  s.email     = 'hugo@quezmedia.com'
  s.homepage  = 'https://bitbucket.org/quezmedia-ondemand/solidus_batt_certificate/'

  s.files        = `git ls-files`.split("\n")
  s.test_files   = `git ls-files -- spec/*`.split("\n")
  s.require_path = "lib"
  s.requirements << "none"

  solidus_version = [">= 1.0.6", "< 3"]

  s.add_dependency 'solidus_core', solidus_version

  s.add_dependency 'deface', '~> 1.0'
  s.add_dependency 'fog'
  s.add_dependency 'paperclip'

  s.add_development_dependency "solidus_backend", solidus_version
  s.add_development_dependency "solidus_frontend", solidus_version
  s.add_development_dependency 'capybara'
  s.add_development_dependency "shoulda-matchers"
  s.add_development_dependency 'poltergeist'
  s.add_development_dependency "rspec-rails", "~> 3.3"
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-rspec'
  s.add_development_dependency 'pry-rails', '~> 0.3.4'
end
