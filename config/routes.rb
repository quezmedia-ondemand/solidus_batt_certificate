Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :products do
      resources :certificates, only: [:index, :create]
    end
  end
end
