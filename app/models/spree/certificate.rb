module Spree
  class Certificate < Spree::Base
    belongs_to :owner, polymorphic: true, inverse_of: :certificate, required: true
    belongs_to :user, required: true, class_name: Spree.user_class
    after_save :touch_product

    has_attached_file :attachment,
                      default_url: 'noimage/:style.png',
                      url: '/spree/products/:id/certificate/:style/:basename.:extension',
                      path: ':rails_root/public/spree/products/:id/certificate/:style/:basename.:extension'

    validates_attachment :attachment,
      content_type: { content_type: %w(application/pdf) }

    validates :name, presence: true

    default_scope ->(){ order(position: :asc) }
    scope :available, ->(){ where(available: true) }

    private
    def touch_product
      owner.update_column(:updated_at, Time.current)
    end
  end
end
