module Spree
  Product.class_eval do
    has_many :certificates,                  class_name: 'Spree::Certificate',
                                             as: :owner,
                                             inverse_of: :owner,
                                             autosave: true

    accepts_nested_attributes_for :certificates, reject_if: :all_blank, allow_destroy: true
  end
end
