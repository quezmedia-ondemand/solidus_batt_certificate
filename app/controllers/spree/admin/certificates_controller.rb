module Spree
  module Admin
    class CertificatesController < Spree::Admin::BaseController
      before_action :find_product

      def index
      end

      def create
        if @product.update(certificate_params)
          respond_with(@product) do |format|
            format.html {
              redirect_to(admin_product_certificates_path(product_id: @product.id),
                flash: {success: Spree.t(:updated_certificates)})
            }
          end
        else
          render :index
        end
      end

      private
      def find_product
        @product = Spree::Product.friendly.find(params[:product_id])
      end

      def certificate_params
        params.require(:product).permit(
          certificates_attributes: [:id, :_destroy, :available,
            :name, :attachment, :user_id, :owner_type, :owner_id,
            :position])
      end
    end
  end
end
