Deface::Override.new(:virtual_path => "spree/admin/shared/_product_tabs",
                     :name => "product_certificate_admin_product_tabs",
                     :insert_bottom => "[data-hook='admin_product_tabs']",
                     :partial => "spree/admin/shared/product_certificates_product_tabs",
                     :original => 'c9d57831881e02625bdb071ac75aa8e07a40f18a',
                     :disabled => false)
