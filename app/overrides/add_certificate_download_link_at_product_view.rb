Deface::Override.new(:virtual_path => "spree/products/show",
                     :name => "product_certificate_file_download",
                     :insert_after => "[data-hook='product_images']",
                     :partial => "spree/products/certificate_download",
                     :disabled => false)
