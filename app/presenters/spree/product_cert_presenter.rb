module Spree
  class ProductCertPresenter < SimpleDelegator
    def self.decorate_collection(products)
      products.map { |p| new(p) }
    end

    def object
      __getobj__
    end
  end
end
